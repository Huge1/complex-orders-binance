# Taken from https://github.com/sclorg/s2i-python-container/blob/master/examples/pipenv-test-app/setup.py , meant for OCP/Origin, will be of little use here.
from setuptools import setup, find_packages

setup (
    name             = "complex-orders-binance",
    version          = "0.0.4",
    description      = "Handling orders to Binance, stub",
    packages         = find_packages(),
    install_requires = ["gunicorn", "flask", "ccxt"],
)
