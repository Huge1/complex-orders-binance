# --- Release with Alpine ----
FROM python:3.6-alpine3.7
# Create app directory
WORKDIR /app

#suboptimal is good enough here...
COPY  /app/ ./
RUN pip install -r requirements.txt
# Well, I know the built-in wsgi server is lame, but I did have too litle time to optimize for the Gunicorn or WSGI../
#Using flask 'binary' was not a good option as that only made it run 5000 and locally!!
#ENV FLASK_APP webAppBackendBasicFlask.py
#ENV FLASK_ENV production
#EXPOSE 5000
#CMD ["flask", "run"]
#This is better startup:
CMD ["python", "webAppBackendBasicFlask.py"]
EXPOSE 80
